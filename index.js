// Declare 3 global variables without initialization called username,password and role.

let username;
let password;
let role;

// Create a login function which is able to prompt the user to provide their username, password and role.

// use prompt() and update the username,password and role global variables with the prompt() returned values.

function login() {
  username = prompt("Enter your username:");
  password = prompt("Enter your password:");
  role = prompt("Enter your role (admin, teacher, or student):");

// add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.

  if (username === "" || username === null || password === "" || password === null || role === "" || role === null) {

// if it is, show an alert to inform the user that their input should not be empty.

    alert("Input should not be empty");

// Add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:

  } else {
    switch (role) {

// if the user's role is admin, show an alert with the following message: "Welcome back to the class portal, admin!"

      case "admin":
        alert("Welcome back to the class portal, admin!");
        break;

// if the user's role is teacher, show an alert with the following message: "Thank you for logging in, teacher!"

      case "teacher":
        alert("Thank you for logging in, teacher!");
        break;

// if the user's role is a student, show an alert with the following message: "Welcome to the class portal, student!"

      case "student":
        alert("Welcome to the class portal, student!");
        break;


// if the user's role does not fall under any of the cases, as a default, show a message: "Role out of range."

      default:
        alert("Role out of range.");
    }
  }
}

login();

// Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.

// add parameters appropriate to describe the arguments.

function calculateAverage(num1, num2, num3, num4) {

// create a new function scoped variable called average.

let average;

// calculate the average of the 4 number inputs and store it in the variable average.

average = (num1 + num2 + num3 + num4) / 4;

// research the use of Math.round() and round off the value of the average variable.

// update the average variable with the use of Math.round()

// console.log() the average variable to check if it is rounding off first.

average = Math.round(average);

// Add an if statement to check if the value of avg is less than or equal to 74.

// if it is, show the following message in a console.log(): "Hello, student, your average is <show average>. The letter equivalent is F"

  if (average <= 74) {
    console.log(`Hello, student, your average is ${average}. The letter equivalent is F`);

// Add an else if statement to check if the value of avg is greater than or equal to 75 and if average is less than or equal to 79.

// if it is, show the following message in a console.log(): "Hello, student, your average is <show average>. The letter equivalent is D"

  } else if (average >= 75 && average <= 79) {
    console.log(`Hello, student, your average is ${average}. The letter equivalent is D`);

// Add an else if statement to check if the value of avg is greater than or equal to 80 and if average is less than or equal to 84.

// if it is, show the following message in a console.log(): "Hello, student, your average is <show average>. The letter equivalent is C"

  } else if (average >= 80 && average <= 84) {
    console.log(`Hello, student, your average is ${average}. The letter equivalent is C`);

// Add an else if statement to check if the value of avg is greater than or equal to 85 and if average is less than or equal to 89.

// if it is, show the following message in a console.log(): "Hello, student, your average is <show average>. The letter equivalent is B"

  } else if (average >= 85 && average <= 89) {
    console.log(`Hello, student, your average is ${average}. The letter equivalent is B`);

// Add an else if statement to check if the value of avg is greater than or equal to 90 and if average is less than or equal to 95.

// if it is, show the following message in a console.log(): "Hello, student, your average is <show average>. The letter equivalent is A"

  } else if (average >= 90 && average <= 95) {
    console.log(`Hello, student, your average is ${average}. The letter equivalent is A`);

// Add an else if statement to check if the value of average is greater than 96.

// if it is, show the following message in a console.log(): "Hello, student, your average is <show average>. The letter equivalent is A+"

  } else if (average > 96) {
    console.log(`Hello, student, your average is ${average}. The letter equivalent is A+`);
  }
}

calculateAverage(71, 70, 73, 74);
calculateAverage(75, 75, 76, 78);
calculateAverage(80, 81, 82, 78);
calculateAverage(84, 85, 87, 88);
calculateAverage(89, 90, 91, 90);
calculateAverage(91, 96, 97, 95);
